before(function() {
  cy.clearLocalStorage();
});

it("Visits my local host", function() {
  cy.visit("/");
});

it("Change to Bot view", function() {
  cy.get(".change-page").click();

  cy.get(".bot-form").should("have.length", 1);
});

before(function() {
  cy.clearLocalStorage();
});

it("Visits my local host", function() {
  cy.visit("/");
});

it("Makes a post", function() {
  cy.get('input[name="title"]')
    .type("New post")
    .should("have.value", "New post");

  cy.get('textarea[name="content"]')
    .type("Writing a new post")
    .should("have.value", "Writing a new post");

  cy.get('input[value="Create"]').click();
  
  cy.get(".single-post > article > h2").should("contain", "New post");
});

it("Deletes a post", function() {
  cy.get('.bg-red-dark').click();

  cy.get(".single-post")
    .should("have.length", 0);
});

import React from "react";
import { mount } from "enzyme";
import SinglePost from "../components/SinglePost";

test("should show delete button when author = currentPersona", () => {
  const wrapper = mount(
    <SinglePost
      title=""
      content="true"
      id=""
      author="Valesca"
      currentPersona="Valesca"
      date="true"
      onClick={() => {}}
    />
  );
  expect(wrapper.find("button")).toHaveLength(1);
});

test("should call function on click", () => {
  const onClickMock = jest.fn();
  const wrapper = mount(
    <SinglePost
      title=""
      content=""
      id=""
      author=""
      currentPersona=""
      date=""
      onClick={onClickMock}
    />
  );
  wrapper.find("button").simulate("click");
  expect(onClickMock).toHaveBeenCalled();
});

test("should have Comments title", () => {
  const wrapper = mount(
    <SinglePost
      title=""
      content=""
      id=""
      author=""
      currentPersona=""
      date=""
      onClick={() => {}}
    />
  );
  expect(wrapper.find(".text-indigo-darker").text()).toContain("Comments");
});

import React from "react";
import { mount } from "enzyme";
import CreateNewPost from "../components/CreateNewPost";
import * as api from "../api/index"

test("update state on title input change", () => {
  const wrapper = mount(<CreateNewPost updatePosts={() => {}} author="" />);
  expect(wrapper.state().title).toBe("")
  wrapper.find('[name="title"]').simulate("change", {
    target: { name: "title", value: "test title" }
  });
  expect(wrapper.state().title).toBe("test title");
});

test("update state on content i textarea change", () => {
  const wrapper = mount(<CreateNewPost updatePosts={() => {}} author="" />);
  expect(wrapper.state().content).toBe("")
  wrapper.find('[name="content"]').simulate("change", {
    target: { name: "content", value: "test content" }
  });
  expect(wrapper.state().content).toBe("test content");
});

test("CreateNewPost on submit", () => {
  const wrapper = mount(<CreateNewPost updatePosts={() => {}} author="" />);
  expect(api.fetchAllPosts()).toHaveLength(0);
  wrapper.find('[type="submit"]').simulate("submit");
  expect(api.fetchAllPosts()).toHaveLength(1);
});
import React from "react";
import { shallow } from "enzyme";
import Button from "../components/Button";

test("button should change style", () => {
  const wrapper = shallow(
    <Button danger={true} onClick={() => {}} className="">
      <div></div>
    </Button>
  );
  expect(wrapper.find("button").hasClass("bg-red-dark")).toEqual(true);
});

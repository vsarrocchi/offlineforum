import React from "react";
import { mount } from "enzyme";
import CreateNewComment from "../components/CreateNewComment";
import * as api from "../api/index";

test("update state on title input change", () => {
  const wrapper = mount(
    <CreateNewComment postId="" author="" updateComments={() => {}} />
  );
  expect(wrapper.state().comment).toBe("");
  wrapper.find("textarea").simulate("change", {
    target: { name: "comment", value: "test comment" }
  });
  expect(wrapper.state().comment).toBe("test comment");
});

test("CreateNewComment on submit", () => {
  const wrapper = mount(
    <CreateNewComment postId="" author="" updateComments={() => {}} />
  );
  expect(api.fetchAllComments()).toHaveLength(0);
  wrapper.find('[type="submit"]').simulate("submit");
  expect(api.fetchAllComments()).toHaveLength(1);
});

import React from "react";
import { mount } from "enzyme";
import Posts from "../components/Posts";

test("shoul have a form to create new post", () => {
  const wrapper = mount(<Posts currentPersona="" />);
  expect(wrapper.find("form")).toHaveLength(1);
});

test("should render a post list", () => {
  const wrapper = mount(<Posts currentPersona="" />);
  wrapper.setState({
    posts: [
      { title: "", content: "", id: "1", author: "", date: "" },
      { title: "", content: "", id: "2", author: "", date: "" },
      { title: "", content: "", id: "3", author: "", date: "" }
    ]
  });
  expect(wrapper.find("article")).toHaveLength(3);
});

test("should call removePost on click", () => {
  const wrapper = mount(<Posts currentPersona="Valesca" />);
  wrapper.setState({
    posts: [
      { title: "", content: "", id: "1", author: "Milka", date: "" },
      { title: "", content: "", id: "2", author: "Valesca", date: "" },
      { title: "", content: "", id: "3", author: "Danilo", date: "" },
      { title: "", content: "", id: "4", author: "Valesca", date: "" }
    ]
  });
  expect(wrapper.find("article")).toHaveLength(4);
  expect(wrapper.find("button")).toHaveLength(2);
  // wrapper.find("button").simulate("click");
  // expect(wrapper.find("article")).toHaveLength(3);
});

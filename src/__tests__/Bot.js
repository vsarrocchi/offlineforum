import React from "react";
import { mount } from "enzyme";
import Bot from "../components/Bot/Bot";
import * as api from "../api/index";

test("should have a message form", () => {
  const wrapper = mount(<Bot />);
  expect(wrapper.find("form")).toHaveLength(1);
});

test("should render messages", () => {
  const wrapper = mount(<Bot />);
  wrapper.setState({
    messages: [
      { message: "text1", bot: false },
      { message: "text2", bot: false }
    ]
  });
  expect(wrapper.find("p")).toHaveLength(2);
});

test("should send message on submit", () => {
  const wrapper = mount(<Bot />);
  wrapper.instance().onSubmit("text1");
  expect(wrapper.state().messages[0].message).toMatch("text1");
});

test("should return bot reply", async () => {
  const wrapper = mount(<Bot />);
  api.botReply = await jest.fn(function botReply() {
    return new Promise((resolve, reject) => {
      resolve({ message: "test reply", bot: true });
    });
  });
  wrapper.instance().onSubmit("test message");
  expect(api.botReply).toHaveBeenCalled();
  // expect(wrapper.state().messages).toHaveLength(2);
});

import React from "react";
import { mount } from "enzyme";
import Comments from "../components/Comments";

test("shoul have a form to create new comment", () => {
  const wrapper = mount(<Comments postId="" currentPersona="" />);
  expect(wrapper.find("form")).toHaveLength(1);
});

test("should render a comment list", () => {
  const wrapper = mount(<Comments postId="" currentPersona="" />);
  wrapper.setState({
    comments: [
      { id: "1", author: "", comment: "", date: "" },
      { id: "2", author: "", comment: "", date: "" },
      { id: "3", author: "", comment: "", date: "" }
    ]
  });
  expect(wrapper.find(".text-grey-dark")).toHaveLength(3);
});

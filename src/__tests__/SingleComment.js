import React from "react";
import { mount } from "enzyme";
import SingleComment from "../components/SingleComment";

test("should show delete button when author = currentPersona", () => {
  const wrapper = mount(
    <SingleComment
      id=""
      author="Valesca"
      onClick={() => {}}
      currentPersona="Valesca"
      comment=""
      date=""
    />
  );
  expect(wrapper.find("button")).toHaveLength(1);
});

test("should remove comment on click", () => {
  const wrapper = mount(
    <SingleComment
      id=""
      author=""
      onClick={() => {}}
      currentPersona=""
      comment=""
      date=""
    />
  );
  wrapper.find("button").simulate("click");
});
